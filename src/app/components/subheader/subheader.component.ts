import { Component, OnInit } from '@angular/core';
import { CommonService } from './../../services/common.service';

@Component({
  selector: 'app-subheader',
  templateUrl: './subheader.component.html',
  styleUrls: ['./subheader.component.css']
})
export class SubheaderComponent implements OnInit {

  constructor(public common: CommonService) { }

  ngOnInit(): void {
  }

    
open(menu){
  menu.openMenu();
  }

}
