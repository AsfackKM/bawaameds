import { Component, OnInit,ViewChild } from '@angular/core';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';
import { NgImageSliderComponent } from 'ng-image-slider';
import { CommonService } from './../../services/common.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    
@ViewChild('nav') slider: NgImageSliderComponent;
  constructor(public common: CommonService) { }

  ngOnInit(): void {
  }

  prevImageClick() {
    this.slider.prev();
}

nextImageClick() {
    this.slider.next();
}

}
