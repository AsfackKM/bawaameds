import { Component, OnInit } from '@angular/core';
import { CommonService } from './../../services/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
// Slider Images
slides = [{'image': 'assets/common/slider/slider1.jpg'}, {'image': 'assets/common/slider/slider2.jpg'},{'image': 'assets/common/slider/slider3.jpg'}, {'image': 'assets/common/slider/slider4.jpg'}];
slides1 = [{'path': 'assets/common/slider/slider1.jpg'}, {'path': 'assets/common/slider/slider2.jpg'},{'path': 'assets/common/slider/slider3.jpg'}];
customHight = '300px';
imageObject = [{
  image: 'assets/common/slider/customSlider1/slider1.jpg',
  thumbImage: 'assets/common/slider/customSlider1/slider1.jpg'
},{
image: 'assets/common/slider/customSlider1/slider2.jpg',
thumbImage: 'assets/common/slider/customSlider1/slider2.jpg'
},{
image: 'assets/common/slider/customSlider1/slider3.jpg',
thumbImage: 'assets/common/slider/customSlider1/slider3.jpg'
},{
image: 'assets/common/slider/customSlider1/slider4.jpg',
thumbImage: 'assets/common/slider/customSlider1/slider4.jpg'
},{
image: 'assets/common/slider/customSlider1/slider5.jpg',
thumbImage: 'assets/common/slider/customSlider1/slider5.jpg'
},];

paymentImage = [{
  image: 'assets/common/icons/Amazon_Logo.png',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/digibank_logo.png',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/freecharge.png',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/Mobikwik_logo.png',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
}];
popularImage = [{
  image: 'assets/common/icons/netmeds-immunity-care.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/netmeds-immunity-care.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/netmeds-immunity-care.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/netmeds-immunity-care.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
}];

shopImage = [{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Essentium Phygen 5-Layer N95 Face Mask without Valve (USP 2500)',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
},{
  image: 'assets/common/icons/shop1.jpg',
  title: 'Up to Rs. 300 Amazon Pay Cashback*',
  subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..'
}];

checkDevice = 3;
  constructor(public common: CommonService) { }

  ngOnInit(): void {
    this.checkDevice = this.common.checkDevice();
  }   
  
}
