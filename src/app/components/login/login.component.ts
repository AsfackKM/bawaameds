import { AfterViewInit, Component, OnInit } from '@angular/core';
import { FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { SocialAuthService } from "angularx-social-login";
import { WindowService } from '../../services/window.service';
import firebase from 'firebase/app';
import 'firebase/auth';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit , AfterViewInit{
  windowRef: any;

  phoneNumber;

  verificationCode: string;

  users: any;

  user: SocialUser;
  isShowAccount = false;
  recaptchaVerifier;

  constructor(private win: WindowService, public authService: SocialAuthService) { }

  ngOnInit(): void {
    this.windowRef = this.win.windowRef
    // this.authService.authState.subscribe((user) => {
    //   this.user = user;
    //   console.log(this.user);
    // });
  }

  ngAfterViewInit(){
    this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', {
      'size': 'normal',
      'callback': function (response) {
        console.log("success", response);
      }      
    });
    this.windowRef.recaptchaVerifier.render()    
  }

  sendLoginCode() {  
    let num = "+91" + this.phoneNumber;
    console.log("success", num);
    firebase.auth().signInWithPhoneNumber(num, this.windowRef.recaptchaVerifier)
      .then(result => {

        this.windowRef.confirmationResult = result;

      })
      .catch(error => console.log("aasfack : ", error));

  }

  verifyLoginCode() {
    this.windowRef.confirmationResult
      .confirm(this.verificationCode)
      .then(result => {

        this.users = result.user;
        console.log("this.users :", this.users);

      })
      .catch(error => console.log(error, "Incorrect code entered?"));
  }

  public socialSignIn(socialPlatform: string) {
    let socialPlatformProvider;
    if (socialPlatform == "facebook") {
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    } else if (socialPlatform == "google") {
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    }

    this.authService.signIn(socialPlatformProvider).then((socialusers) => {
      console.log(socialPlatform + " sign in data : ", socialusers);
      localStorage.setItem('socialusers', JSON.stringify(socialusers));
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  CreateAccount() {
    this.isShowAccount = true;
    this.sendLoginCode();
  }

}