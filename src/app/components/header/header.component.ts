import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CommonService } from './../../services/common.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(public route: Router,public common: CommonService) {

  window.onorientationchange = function()
  {
     window.location.reload();
  };
   }

  ngOnInit(): void {
    console.log(this.route.url)
  }

  showHeader() {
    return this.route.url=='/login'?true:false;
  }

}
