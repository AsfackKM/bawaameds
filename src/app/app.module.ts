import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './modules/angular-material/angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { CustomerProfileComponent } from './components/customer-profile/customer-profile.component';
import { UploadPrescriptionComponent } from './components/upload-prescription/upload-prescription.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { ProductComponent } from './components/product/product.component';
import { CartComponent } from './components/cart/cart.component';
import { SubheaderComponent } from './components/subheader/subheader.component';
import { MatCarouselModule } from '@ngmodule/material-carousel';
import { NgImageSliderModule } from 'ng-image-slider';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IvyCarouselModule } from 'angular-responsive-carousel';
import { SocialLoginModule, SocialAuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login';
import firebase from 'firebase/app';
import { FormsModule } from '@angular/forms';

var firebaseConfig = {
  apiKey: "AIzaSyAsxKwnQIDa1MKYJ-u0cXsY64M3hYd16tY",
  authDomain: "bawaameds.firebaseapp.com",
  projectId: "bawaameds",
  storageBucket: "bawaameds.appspot.com",
  messagingSenderId: "104075379235",
  appId: "1:104075379235:web:c96f3c0f3285c580832796",
  measurementId: "G-78SWRS236S"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);  

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    CustomerProfileComponent,
    UploadPrescriptionComponent,
    CategoriesComponent,
    ProductComponent,
    CartComponent,
    SubheaderComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    SocialLoginModule,
    AppRoutingModule,
    AngularMaterialModule,
    FlexLayoutModule,
    MatCarouselModule.forRoot(),
    NgImageSliderModule,
    IvyCarouselModule,
    BrowserAnimationsModule
  ],
  providers: [{
    provide: 'SocialAuthServiceConfig',
    useValue: {
      autoLogin: false,
      providers: [
        {
          id: GoogleLoginProvider.PROVIDER_ID,
          provider: new GoogleLoginProvider(
            '104075379235-mqi5gmir3hdptqnbjqfabb1pdl975l30.apps.googleusercontent.com'
          )
        },
        {
          id: FacebookLoginProvider.PROVIDER_ID,
          provider: new FacebookLoginProvider('1089141468192023')
        }
      ]
    } as SocialAuthServiceConfig,
    // useFactory: provideConfig
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }