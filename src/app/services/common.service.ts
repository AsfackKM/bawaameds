import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(public route: Router) { }

  subSubMenu = [
    {
      titile:'COVID Essentials',
      url:''
    },
    {
      titile:'Diabetes',
      url:''
    },
    {
      titile:'Eyewear',
      url:''
    },
    {
      titile:'Ayush',
      url:''
    },
    {
      titile:'Fitness',
      url:''
    },
    {
      titile:'Personal Care',
      url:''
    },
    {
      titile:'Mom & Baby',
      url:''
    },
    {
      titile:'Devices',
      url:''
    },
    {
      titile:'Surgicals',
      url:''
    },
    {
      titile:'Sexual Wellness',
      url:''
    },
    {
      titile:'Treatments',
      url:''
    },
  ];
  showHeader() {
    return this.route.url=='/login'?true:false;
  }
  showMainContent() {
    return this.route.url=='/'?true:false;
  }

  checkDevice() {
    if(/Android|webOS|iPhone|BlackBerry|IEMobile|Opera Mini|Mobile|mobile|CriOS/i.test(navigator.userAgent)) {
      return 1;
    }else if(/iPad|iPod/i.test(navigator.userAgent)) {
      return 2;
    }else{
      return 3;
    }
  }

  commonSlider = [
    {
      'section' :'SHOP NOW',
      'title' : 'COVID Essentials| Up to 80% Off',
      'subtitle' : 'Stay Safe',
      'sliders' : [
        {
          image: 'assets/common/icons/shop1.jpg',
          title: 'Essentium Phygen 5-Layer N95 Face Mask without Valve (USP 2500)',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        },{
          image: 'assets/common/icons/shop1.jpg',
          title: 'Up to Rs. 300 Amazon Pay Cashback*',
          subtitle: 'Get “Assured Cashback” between max. Rs. 300 and min. Rs. 15 as Amazon Pay balance on a minimum p..',
          heading: ''
        }
      ]
    },
    {
      'section' :'STOCK UP',
      'title' : 'Immunity Boosters',
      'subtitle' : '',
      'sliders' : [
        {
          image: 'assets/common/icons/aadar_immunity.jpg',
          title: 'AADAR Immunity Pro 500 mg Veg Capsule 60\'s- Ayurvedic Immunity boosters for Adults',
          subtitle: 'Mfr: Aadar',
          heading: ''
        },{
          image: 'assets/common/icons/lama_immunity_kit.jpg',
          title: 'Lama Immunity Kit',
          subtitle: 'Mfr: Inlife Pharma Pvt Ltd',
          heading: ''
        },{
          image: 'assets/common/icons/aadar_immunity.jpg',
          title: 'AADAR Immunity Pro 500 mg Veg Capsule 60\'s- Ayurvedic Immunity boosters for Adults',
          subtitle: 'Mfr: Aadar',
          heading: ''
        },{
          image: 'assets/common/icons/lama_immunity_kit.jpg',
          title: 'Lama Immunity Kit',
          subtitle: 'Mfr: Inlife Pharma Pvt Ltd',
          heading: ''
        },{
          image: 'assets/common/icons/aadar_immunity.jpg',
          title: 'AADAR Immunity Pro 500 mg Veg Capsule 60\'s- Ayurvedic Immunity boosters for Adults',
          subtitle: 'Mfr: Aadar',
          heading: ''
        },{
          image: 'assets/common/icons/lama_immunity_kit.jpg',
          title: 'Lama Immunity Kit',
          subtitle: 'Mfr: Inlife Pharma Pvt Ltd',
          heading: ''
        },{
          image: 'assets/common/icons/aadar_immunity.jpg',
          title: 'AADAR Immunity Pro 500 mg Veg Capsule 60\'s- Ayurvedic Immunity boosters for Adults',
          subtitle: 'Mfr: Aadar',
          heading: ''
        },{
          image: 'assets/common/icons/lama_immunity_kit.jpg',
          title: 'Lama Immunity Kit',
          subtitle: 'Mfr: Inlife Pharma Pvt Ltd',
          heading: ''
        },{
          image: 'assets/common/icons/aadar_immunity.jpg',
          title: 'AADAR Immunity Pro 500 mg Veg Capsule 60\'s- Ayurvedic Immunity boosters for Adults',
          subtitle: 'Mfr: Aadar',
          heading: ''
        },{
          image: 'assets/common/icons/lama_immunity_kit.jpg',
          title: 'Lama Immunity Kit',
          subtitle: 'Mfr: Inlife Pharma Pvt Ltd',
          heading: ''
        },{
          image: 'assets/common/icons/aadar_immunity.jpg',
          title: 'AADAR Immunity Pro 500 mg Veg Capsule 60\'s- Ayurvedic Immunity boosters for Adults',
          subtitle: 'Mfr: Aadar',
          heading: ''
        },{
          image: 'assets/common/icons/lama_immunity_kit.jpg',
          title: 'Lama Immunity Kit',
          subtitle: 'Mfr: Inlife Pharma Pvt Ltd',
          heading: ''
        },
      ]
    },
    {
      'section' :'EXPLORE',
      'title' : 'Popular Categories ',
      'subtitle' : 'Get best deals on wellness products',
      'sliders' : [
        {
          image: 'assets/common/icons/shampoos_and_conditioners_1.jpg',
          title: '',
          subtitle: '',
          heading: 'Shampoos And Conditioners'
        },{
          image: 'assets/common/icons/creams_oils_lotions.jpg',
          title: '',
          subtitle: '',
          heading: 'Creams/Oils/Lotions'
        },{
          image: 'assets/common/icons/shampoos_and_conditioners_1.jpg',
          title: '',
          subtitle: '',
          heading: 'Shampoos And Conditioners'
        },{
          image: 'assets/common/icons/creams_oils_lotions.jpg',
          title: '',
          subtitle: '',
          heading: 'Creams/Oils/Lotions'
        },{
          image: 'assets/common/icons/shampoos_and_conditioners_1.jpg',
          title: '',
          subtitle: '',
          heading: 'Shampoos And Conditioners'
        },{
          image: 'assets/common/icons/creams_oils_lotions.jpg',
          title: '',
          subtitle: '',
          heading: 'Creams/Oils/Lotions'
        },{
          image: 'assets/common/icons/shampoos_and_conditioners_1.jpg',
          title: '',
          subtitle: '',
          heading: 'Shampoos And Conditioners'
        },{
          image: 'assets/common/icons/creams_oils_lotions.jpg',
          title: '',
          subtitle: '',
          heading: 'Creams/Oils/Lotions'
        },{
          image: 'assets/common/icons/shampoos_and_conditioners_1.jpg',
          title: '',
          subtitle: '',
          heading: 'Shampoos And Conditioners'
        },{
          image: 'assets/common/icons/creams_oils_lotions.jpg',
          title: '',
          subtitle: '',
          heading: 'Creams/Oils/Lotions'
        },
      ]
    }
  ];

}
