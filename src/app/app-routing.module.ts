import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CartComponent } from './components/cart/cart.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { CustomerProfileComponent } from './components/customer-profile/customer-profile.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { ProductComponent } from './components/product/product.component';
import { UploadPrescriptionComponent } from './components/upload-prescription/upload-prescription.component';

const routes: Routes = [
  {
    path: "", component: HomeComponent,
    children: [
      {
        path: 'category', component: CategoriesComponent
      },
      { path: 'product', component: ProductComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
  { path: 'login', component: LoginComponent },
  { path: 'category', component: CategoriesComponent },
  { path: 'cart', component: CartComponent },
  { path: 'customer-profile', component: CustomerProfileComponent },
  { path: 'upload-prescription', component: UploadPrescriptionComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
